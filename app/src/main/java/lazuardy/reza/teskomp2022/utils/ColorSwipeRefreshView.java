package lazuardy.reza.teskomp2022.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import lazuardy.reza.teskomp2022.R;

public class ColorSwipeRefreshView extends SwipeRefreshLayout {

    public ColorSwipeRefreshView(@NonNull Context context) {
        super(context);
    }

    public ColorSwipeRefreshView(
            @NonNull Context context,
            @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyColor(attrs);
    }

    private void applyColor(@Nullable AttributeSet attrs) {
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ColorSwipeRefresh,
                0, 0);

        int color;
        try {
            color = a.getColor(R.styleable.ColorSwipeRefresh_loaderColor, Color.DKGRAY);
        } finally {
            a.recycle();
        }

        setColorSchemeColors(color);
    }
}

