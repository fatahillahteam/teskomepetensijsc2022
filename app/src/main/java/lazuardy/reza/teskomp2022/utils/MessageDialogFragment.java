package lazuardy.reza.teskomp2022.utils;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

public class MessageDialogFragment extends DialogFragment {

    public final static String TITLE_ARG = "title";
    public final static String MESSAGE_ARG = "message";

    private String title;
    private String message;

    public static MessageDialogFragment newInstance(String message) {
        return newInstance(null, message);
    }

    public static MessageDialogFragment newInstance(String title, String message) {

        Bundle args = new Bundle();

        MessageDialogFragment fragment = new MessageDialogFragment();
        fragment.setArguments(args);
        args.putString(MESSAGE_ARG, message);
        args.putString(TITLE_ARG, title);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            this.title = args.getString(TITLE_ARG);
            this.message = args.getString(MESSAGE_ARG);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = this.title == null ? "Info" : this.title;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialogInterface, i) -> { });

        return builder.create();
    }

    /** Show dialog if parent fragment resumed, or ignore if fragment is paused.
     *
     * @param parent
     * @param tag
     */
    public void showIfResumed(@Nullable Fragment parent, String tag) {
        // it is to prevent IllegalStateException when showing dialog while activity resumed
        if (parent != null && parent.isResumed()) {
            show(parent.getParentFragmentManager(), tag);
        } else {

        }
    }

}