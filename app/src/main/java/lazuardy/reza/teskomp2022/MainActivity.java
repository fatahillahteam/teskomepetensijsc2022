package lazuardy.reza.teskomp2022;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import lazuardy.reza.teskomp2022.databinding.ActivityMainBinding;
import lazuardy.reza.teskomp2022.view.UserProfileListFragment;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBar.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setTitle("Pilih Kontak");

        initFragment();
    }

    private void initFragment() {
        FragmentManager manager = getSupportFragmentManager();
        UserProfileListFragment userProfileListFragment = (UserProfileListFragment) manager.findFragmentByTag("pengantar");
        if (userProfileListFragment == null) {
            userProfileListFragment = userProfileListFragment.newInstance();
            manager.beginTransaction()
                    .replace(R.id.content_view, userProfileListFragment, "Pilih Kontak")
                    .commit();
        }
    }
}