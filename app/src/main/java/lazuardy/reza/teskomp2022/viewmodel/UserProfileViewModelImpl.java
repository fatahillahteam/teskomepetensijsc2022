package lazuardy.reza.teskomp2022.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import lazuardy.reza.teskomp2022.model.UserProfile;
import lazuardy.reza.teskomp2022.model.UserProfileViewState;
import lazuardy.reza.teskomp2022.repository.Repository;
import lazuardy.reza.teskomp2022.repository.RepositoryImpl;
import lazuardy.reza.teskomp2022.repository.RepositoryListener;

public class UserProfileViewModelImpl extends AndroidViewModel implements UserProfileViewModel  {

    private final MutableLiveData<UserProfileViewState> listUserProfileViewState;


    private final Repository repository;

    private List<UserProfile> listUserProfileCache;

    /*
     *  AndroidViewModel must have constructor
     *  with exactly one "application" argument.
     */
    public UserProfileViewModelImpl(Application application) {
        // invoke other constructor with our default repository
        this(application, new RepositoryImpl(application));
    }

    /*
     *  Constructor with repository argument,
     *  so we can inject different repository implementation for testing.
     */
    public UserProfileViewModelImpl(Application application, RepositoryImpl repository) {
        super(application);
        this.repository = repository;

        listUserProfileViewState = new MutableLiveData<>();

    }



    @Override
    public LiveData<UserProfileViewState> getListUserProfile() {
        // load data if not loaded yet (view state value is null)
        if (listUserProfileViewState.getValue() == null) {
        }
        return listUserProfileViewState;
    }

    @Override
    public void refreshListUserProfile() {
        // update view state to show progress
        listUserProfileViewState.postValue(UserProfileViewState.progressState());
        repository.getProfileList(new RepositoryListener<List<UserProfile>>() {

            @Override
            public void onSuccess(@NonNull List<UserProfile> data) {
                listUserProfileCache = data;
                listUserProfileViewState.postValue(UserProfileViewState.userProfileViewState(listUserProfileCache));
            }

            @Override
            public void onError(@NonNull String message) {
                // update view state to show error
                listUserProfileViewState.postValue(UserProfileViewState.errorState(message));
            }
        });
    }
}
