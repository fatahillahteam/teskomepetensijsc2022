package lazuardy.reza.teskomp2022.viewmodel;

import androidx.lifecycle.LiveData;

import java.util.List;

import lazuardy.reza.teskomp2022.model.UserProfileViewState;

public interface UserProfileViewModel {

    /** Get observable view state.
     *
     */

    LiveData<UserProfileViewState> getListUserProfile();

    void refreshListUserProfile();
}
