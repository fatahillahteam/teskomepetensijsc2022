package lazuardy.reza.teskomp2022.model;

import java.util.List;

/**
 * Represent contact list view state.
 * <p>
 * This class is immutable view state, so to change view state you must create
 * new instance, optionally using given factory method.
 * <p>
 */
public final class UserProfileViewState {

    /*
     * Contact list view can have three view state:
     *
     *     - View state is still loading data, in this state isShowProgress() is true
     *     - View state data has been loaded, in this state hasData() is true
     *     - View state is failed to load data, in this state hasErrorMessage() is true
     *
     * In any other case, it is view responsibility to decide how to present these state to user.
     *
     */

    private final List<UserProfile> userProfileList;
    private final boolean showProgress;
    private final String errorMessage;

    /**
     * Create new view state that represent displaying contact list.
     *
     * @param userProfileList detail market to show.
     */
    public static UserProfileViewState userProfileViewState(List<UserProfile> userProfileList) {
        return new UserProfileViewState(userProfileList, false, null);
    }

    /**
     * Create new view state that represent error.
     *
     * @param errorMessage Error message to show.
     */
    public static UserProfileViewState errorState(String errorMessage) {
        return new UserProfileViewState(null, false, errorMessage);
    }

    /**
     * Represent view state that display progress.
     */
    public static UserProfileViewState progressState() {
        return new UserProfileViewState(null, true, null);
    }

    /**
     * Create view state instance.
     *
     * @param userProfileList
     *      Data.
     * @param showProgress
     *      Whether progress shown.
     * @param errorMessage
     *      Whether there are error message that should be displayed.
     */
    public UserProfileViewState(
            List<UserProfile> userProfileList,
            boolean showProgress,
            String errorMessage) {
        this.userProfileList = userProfileList;
        this.showProgress = showProgress;
        this.errorMessage = errorMessage;
    }

    public List<UserProfile> getUserProfileList() {
        return userProfileList;
    }

    public boolean isShowProgress() {
        return showProgress;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean hasData() {
        return userProfileList != null;
    }

    public boolean hasErrorMessage() {
        return errorMessage != null;
    }
}
