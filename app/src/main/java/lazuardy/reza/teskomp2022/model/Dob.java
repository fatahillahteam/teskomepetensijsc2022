package lazuardy.reza.teskomp2022.model;

import java.io.Serializable;

public class Dob implements Serializable {
    private String date;
    private String age;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
