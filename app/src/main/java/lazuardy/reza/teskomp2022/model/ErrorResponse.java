package lazuardy.reza.teskomp2022.model;

public class ErrorResponse {
    private ErrorData error;
    private ErrorMeta meta;

    public ErrorResponse() {

    }

    public ErrorData getErrorData() {
        return error;
    }

    public void setErrorData(ErrorData errorData) {
        this.error = errorData;
    }

    public ErrorMeta getErrorMeta() {
        return meta;
    }

    public void setErrorMeta(ErrorMeta errorMeta) {
        this.meta = errorMeta;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "error=" + error +
                '}';
    }
}

