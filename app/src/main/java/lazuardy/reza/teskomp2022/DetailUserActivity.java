package lazuardy.reza.teskomp2022;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import lazuardy.reza.teskomp2022.databinding.ActivityDetailUserBinding;
import lazuardy.reza.teskomp2022.databinding.ActivityMainBinding;
import lazuardy.reza.teskomp2022.model.UserProfile;
import lazuardy.reza.teskomp2022.view.UserProfileDetailFragment;
import lazuardy.reza.teskomp2022.view.UserProfileListFragment;

public class DetailUserActivity extends AppCompatActivity {

    private ActivityDetailUserBinding binding;
    private UserProfile userProfile = new UserProfile();
    private final static String USER_PROFILE = "user profile";
    public static Intent newIntent(Context context, UserProfile userProfile) {
        Intent intent = new Intent();
        intent.putExtra(USER_PROFILE, userProfile);
        intent.setClass(context, DetailUserActivity.class);
        return intent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailUserBinding.inflate(getLayoutInflater());
        Intent args = getIntent();
        if (args != null) {
            this.userProfile = (UserProfile) args.getSerializableExtra(USER_PROFILE);

        }
        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBar.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Rincian Kontak");
        binding.appBar.ivShare.setOnClickListener(v->shareClikc());
        initFragment();
    }

    private void initFragment() {
        FragmentManager manager = getSupportFragmentManager();
        UserProfileDetailFragment userProfileDetailFragment = (UserProfileDetailFragment) manager.findFragmentByTag("pengantar");
        if (userProfileDetailFragment == null) {
            userProfileDetailFragment = userProfileDetailFragment.newInstance(userProfile);
            manager.beginTransaction()
                    .replace(R.id.content_view, userProfileDetailFragment, "rincian_kontak")
                    .commit();
        }
    }

    private void shareClikc(){
        String shareValue = userProfile.getName().getFirst()+" "+userProfile.getName().getLast()+","+
                userProfile.getEmail()+","+userProfile.getPhone();
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, shareValue);
        startActivity(Intent.createChooser(intent, "Share"));
    }
}