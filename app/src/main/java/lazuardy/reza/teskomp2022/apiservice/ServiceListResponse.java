package lazuardy.reza.teskomp2022.apiservice;

import java.util.List;

public class ServiceListResponse<T> {

    private List<T> results;

    public ServiceListResponse() {
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }

}
