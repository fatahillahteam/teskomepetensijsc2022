package lazuardy.reza.teskomp2022.apiservice;


import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lazuardy.reza.teskomp2022.model.DynamicType;
import lazuardy.reza.teskomp2022.model.RawGeoJson;

public class GsonFactory {

    /** Generate gson instance with same configuration across app.
     *
     * @return
     */
    public static Gson createGson() {
        Gson gson = new GsonBuilder()
            //.setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            //.setPrettyPrinting()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .registerTypeAdapter(RawGeoJson.class, new RawGeoJsonAdapter())
            .registerTypeAdapter(DynamicType.class, new DynamicTypeAdapter())
            //.serializeNulls()
            .create();
        return gson;
    }

}
