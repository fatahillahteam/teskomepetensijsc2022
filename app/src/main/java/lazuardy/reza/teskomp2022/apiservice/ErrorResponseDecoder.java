package lazuardy.reza.teskomp2022.apiservice;


import android.app.Application;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.stream.MalformedJsonException;

import java.io.EOFException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Map;

import lazuardy.reza.teskomp2022.R;
import lazuardy.reza.teskomp2022.model.ErrorMeta;
import lazuardy.reza.teskomp2022.model.ErrorResponse;
import retrofit2.Call;
import retrofit2.Response;

public class ErrorResponseDecoder {
    private Gson gson;
    private Application context;
    private Map<String,String> codeMap;

    /** Create error message decoder instance.
     *
     * @param context
     *      Use application context to prevent activity context leak.
     * @param gson
     * @param codeMap
     *      Code map translating from error code to error message.
     */
    public ErrorResponseDecoder(Application context, Gson gson, Map<String,String> codeMap) {
        this.gson = gson;
        this.context = context;
        this.codeMap = codeMap;
    }

    public ErrorResponseDecoder(Application context, Map<String,String> codeMap) {
        this(context, null, codeMap);
    }

    /** Get appropriate message from retrofit exception.
     *  Must be used from inside {@link retrofit2.Callback#onFailure(Call, Throwable)}.
     *  Must NOT used to get message from common exception.
     *
     * @param throwable
     * @return
     */
    public String getMessageFromRetrofitException(Throwable throwable) {


        String message;
        try {
            throw throwable;
        } catch (MalformedJsonException | JsonParseException e) {
            message = context.getString(R.string.error_unexpected_response);
        } catch (NullPointerException e) {
            message = context.getString(R.string.error_null_pointer_process_response);
        } catch (SocketTimeoutException e) {
            message = context.getString(R.string.message_connection_timeout);
        } catch (EOFException e) {
            // incomplete or malformed content (EOF while parsing)
            // may also occurs when response is completely empty
            message = context.getString(R.string.error_incomplete_response);
        } catch (IOException e) {
            message = context.getString(R.string.message_connection_problem);
        } catch (Throwable e) {
            message = context.getString(R.string.message_unexpected_error, e.getClass().getSimpleName());
        }
        return message;
    }

    @Nullable
    public <T> T asObject(Response<?> response, Class<T> cls) {
        T result;
        try {
            result = getGson().fromJson(response.errorBody().charStream(), cls);
        } catch (NullPointerException|JsonParseException e) {

            result = null;
        }
        return result;
    }

    /*
    public JSONObject asJsonObject(Response<?> response) {
        JSONObject object;
        try {
            object = new JSONObject(response.errorBody().string());
        } catch (JSONException|NullPointerException e) {
            object = new JSONObject();
        } catch (IOException e) {
            object = new JSONObject();
        }
        return object;
    }
    */

    /** Get error message based on response body.
     *
     * @param response
     *      Reponse to decode.
     * @return
     */
    public String getErrorMessage(Response<?> response) {

        ErrorResponse error = decode(response);
        String message = null;
        String code = null;
        String userMessage;

        if (error != null && error.getErrorData() != null) {
            message = error.getErrorData().getMessage();
            code = error.getErrorData().getCode();
        }

        // Get from code map if available
        userMessage = codeMap.get(code);

        // if not available in code, get from message field
        if (userMessage == null) {
            userMessage = message;
        }

        // otherwise just use generic error message (with http code)
        //
        if (userMessage == null) {
            userMessage = context.getString(R.string.error_non_200_response, response.code());
        }

        return userMessage;
    }

    public String getErrorCode(Response<?> response) {

        ErrorResponse error = decode(response);
        String message = null;
        String code = null;
        String userMessage;
        if (error != null && error.getErrorData() != null) {
            message = error.getErrorData().getMessage();
            code = error.getErrorData().getCode();
        }

        // Get from code map if available
        userMessage = codeMap.get(code);

        // if not available in code, get from message field
        if (userMessage == null) {
            userMessage = code;
        }

        // otherwise just use generic error message (with http code)
        //
        if (userMessage == null) {
            userMessage = context.getString(R.string.error_non_200_response, response.code());
        }

        return userMessage;
    }


    public String getErrorMetaMessage(Response<?> response) {

        ErrorResponse error = decode(response);
        String message = null;
        String code = null;
        String userMessage;

        if (error != null && error.getErrorMeta() != null) {
            message = error.getErrorMeta().getMessage();
            code = error.getErrorMeta().getCode();
        }

        // Get from code map if available
        userMessage = codeMap.get(code);

        // if not available in code, get from message field
        if (userMessage == null) {
            userMessage = message;
        }

        // otherwise just use generic error message (with http code)
        //
        if (userMessage == null) {
            userMessage = context.getString(R.string.error_non_200_response, response.code());
        }

        return userMessage;
    }

    public ErrorMeta getErrorMeta(Response<?> response) {

        ErrorResponse error = decode(response);
        String message = null;
        String code = null;
        String userMessage;

        if (error != null && error.getErrorMeta() != null) {
            message = error.getErrorMeta().getMessage();
            code = error.getErrorMeta().getCode();
        }

        // Get from code map if available
        userMessage = codeMap.get(code);

        // if not available in code, get from message field
        if (userMessage == null) {
            userMessage = message;
        }

        // otherwise just use generic error message (with http code)
        //
        if (userMessage == null) {
            userMessage = context.getString(R.string.error_non_200_response, response.code());
        }

        return error.getErrorMeta();
    }

    @Nullable
    private ErrorResponse decode(Response<?> response) {
        ErrorResponse error;
        try {
            error = getGson().fromJson(response.errorBody().charStream(), ErrorResponse.class);
        } catch (JsonParseException e) {
            error = null;
        }
        return error;
    }

    private Gson getGson() {
        if (gson == null) {
            gson = GsonFactory.createGson();
        }
        return gson;
    }
}
