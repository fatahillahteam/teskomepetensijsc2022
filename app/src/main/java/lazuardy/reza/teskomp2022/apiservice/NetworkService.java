package lazuardy.reza.teskomp2022.apiservice;

import lazuardy.reza.teskomp2022.model.UserProfile;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;

public interface NetworkService {
    // request update number
    @GET("api?results=5&exc=login,registered,i d,nat&nat=us&noinfo")
    Call<ServiceListResponse<UserProfile>> getProfileList();
}
