package lazuardy.reza.teskomp2022.apiservice;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import lazuardy.reza.teskomp2022.model.DynamicType;
import lazuardy.reza.teskomp2022.model.RawGeoJson;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NetworkServicefactory {

    /** Create service with given session and default base URL.
     *
     */
    @NonNull
    public static <T> T createDefaultService(
            @NonNull Context context,
            @NonNull final Class<T> clazz) {

        // Historically, default URL retrieved from resources using context.
        // Currently it is not used, and context has been kept for API compatibility
        String url = "https://randomuser.me/";
        return createService(context, url, clazz);
    }

    /** Create service WITHOUT token with default base URL.
            *
            */
    @NonNull
    public static <T> T createDefaultServiceNoToken(@NonNull Context context, @NonNull final Class<T> clazz) {
        return createDefaultService(context, clazz);
    }

    private static final long SHORT_TIMEOUT = 30 * 1000;

    /** Create retrofit service with base URL.
     *
     */
    @NonNull
    public static <T> T createService(Context context,
            @NonNull String baseUrl,
            @NonNull final Class<T> clazz) {
        OkHttpClient.Builder builder = createClientBuilder();
        return createRetrofitService(builder, baseUrl, clazz);
    }

    // package private
    static <T> T createRetrofitService(
            @NonNull OkHttpClient.Builder builder,
            @NonNull String baseUrl,
            @NonNull Class<T> clazz) {

        Gson gson = GsonFactory.createGson();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                //.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(builder.build())
                .build();

        return retrofit.create(clazz);
    }

    // package private
    @NotNull
    static OkHttpClient.Builder createClientBuilder() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.interceptors().add(interceptor);

//        builder.addInterceptor(new F5Interceptor())
//                .readTimeout(1, TimeUnit.MINUTES)
//                .connectTimeout(1, TimeUnit.MINUTES);

        return builder;
    }


}
