package lazuardy.reza.teskomp2022.adapter;

public interface AdapterListener<T> {
    void onItemClick(T data, int position);
}
