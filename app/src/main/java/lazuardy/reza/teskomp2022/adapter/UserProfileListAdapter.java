package lazuardy.reza.teskomp2022.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import lazuardy.reza.teskomp2022.R;
import lazuardy.reza.teskomp2022.model.UserProfile;

public class UserProfileListAdapter extends RecyclerView.Adapter<UserProfileListAdapter.ViewHolder> {
    private List<UserProfile> list;
    private AdapterListener<UserProfile> listener;

    public UserProfileListAdapter(List<UserProfile> list, AdapterListener<UserProfile> listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_user_profile, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserProfile item = list.get(position);
        holder.bind(item,position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvUser;
        private ImageView ivImageUser;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvUser = itemView.findViewById(R.id.tvUser);
            ivImageUser = itemView.findViewById(R.id.ivImageUser);

            itemView.setOnClickListener(v -> onItemClick());
        }

        private void onItemClick() {
            int pos = getLayoutPosition();
            UserProfile item = list.get(pos);
            listener.onItemClick(item, pos);
        }

        public void bind(UserProfile item,int position) {
            final Context context = itemView.getContext();
            String imageUri = item.getPicture().getThumbnail();
            Picasso.get().load(imageUri).into(ivImageUser);
            tvUser.setText(item.getName().getFirst()+" "+item.getName().getLast());
        }
    }
}
