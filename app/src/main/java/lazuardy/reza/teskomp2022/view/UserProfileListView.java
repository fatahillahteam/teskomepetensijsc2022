package lazuardy.reza.teskomp2022.view;


import java.util.List;

import lazuardy.reza.teskomp2022.model.UserProfile;
import lazuardy.reza.teskomp2022.model.UserProfileViewState;

public interface UserProfileListView {

    /** Show commodity list .
     *
     * @param profileList
     *      Data to show.
     */
    void showData(List<UserProfile> profileList);

    /** Whether progress dialog shown.
     *
     * @param progress
     */
    void showProgress(boolean progress);

    /** Show error message.
     *
     * @param message
     */
    void showMessage(String message);

    /** Apply given view state to this view.
     *
     * @param viewState
     */
    default void apply(UserProfileViewState viewState) {

        // show / hide loader from current state
        showProgress(viewState.isShowProgress());

        // if view state has data, show it
        if (viewState.hasData()) {
            showData(viewState.getUserProfileList());
        }

        // if there are error message, show it
        if (viewState.hasErrorMessage()) {
            showMessage(viewState.getErrorMessage());
        }
    }
}
