package lazuardy.reza.teskomp2022.view;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import lazuardy.reza.teskomp2022.adapter.AdapterListener;
import lazuardy.reza.teskomp2022.adapter.UserProfileListAdapter;
import lazuardy.reza.teskomp2022.databinding.FragmentUserProfileDetailBinding;
import lazuardy.reza.teskomp2022.databinding.FragmentUserProfileListBinding;
import lazuardy.reza.teskomp2022.model.UserProfile;
import lazuardy.reza.teskomp2022.utils.MessageDialogFragment;
import lazuardy.reza.teskomp2022.viewmodel.UserProfileViewModel;
import lazuardy.reza.teskomp2022.viewmodel.UserProfileViewModelImpl;

/**
 * A simple {@link Fragment} subclass.
 */

public class UserProfileDetailFragment extends Fragment {

    private FragmentUserProfileDetailBinding binding;
    private UserProfile dataProfile;
    private final static String USER_PROFILE = "userProfile";


    public static UserProfileDetailFragment newInstance(UserProfile userProfile) {
        Bundle args = new Bundle();
        args.putSerializable(USER_PROFILE, userProfile);
        UserProfileDetailFragment fragment = new UserProfileDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Bundle args = getArguments();
        if (args != null) {
            this.dataProfile = (UserProfile) args.getSerializable(USER_PROFILE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        insertData(dataProfile);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentUserProfileDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void insertData(UserProfile userProfile) {
        String imageUri = userProfile.getPicture().getLarge();
        Picasso.get().load(imageUri).into(binding.ivImageDetail);
        binding.tvName.setText(userProfile.getName().getFirst()+" "+userProfile.getName().getLast());
        binding.tvLahirValue.setText(": "+userProfile.getDob().getDate());
        binding.tvJenisKelaminValue.setText(": "+userProfile.getGender());
        binding.tvEmailValue.setText(": "+userProfile.getEmail());
        binding.tvTelponValue.setText(": "+userProfile.getPhone());
        binding.tvSelulerValue.setText(": "+userProfile.getCell());
        binding.tvAlamatValue.setText(": "+userProfile.getLocation().getStreet().getName()+","
                +userProfile.getLocation().getStreet().getNumber()+","+userProfile.getLocation().getCity()+
                ","+userProfile.getLocation().getCountry()+","+userProfile.getLocation().getPostcode());
        binding.tvLokasiValue.setText(": "+userProfile.getLocation().getCoordinates().getLatitude()+","+
                userProfile.getLocation().getCoordinates().getLongitude());

        binding.tvEmailValue.setOnClickListener(v->sendMail());
        binding.tvTelponValue.setOnClickListener(v->callPhone());
        binding.tvSelulerValue.setOnClickListener(v->callCell());
        binding.ivCopy.setOnClickListener(v->copyNumber());
        binding.tvLokasiValue.setOnClickListener(v->clickLokasi());
    }

    private void sendMail(){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{dataProfile.getEmail()});
        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
        i.putExtra(Intent.EXTRA_TEXT   , "body of email");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            //Toast.makeText(MyActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void callPhone(){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+dataProfile.getPhone()));
        startActivity(intent);
    }

    private void callCell(){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+dataProfile.getCell()));
        startActivity(intent);
    }

    private void copyNumber(){
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(binding.tvName.getText(), binding.tvName.getText());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getContext(), "Nama sudah tersalin", Toast.LENGTH_LONG).show();
    }

    private void clickLokasi(){
        String uri = String.format(Locale.ENGLISH, "geo:%s,%s", dataProfile.getLocation().getCoordinates().getLatitude(),
                dataProfile.getLocation().getCoordinates().getLongitude());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        getContext().startActivity(intent);
    }



}
