package lazuardy.reza.teskomp2022.view;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;


import java.util.List;

import lazuardy.reza.teskomp2022.DetailUserActivity;
import lazuardy.reza.teskomp2022.adapter.AdapterListener;
import lazuardy.reza.teskomp2022.adapter.UserProfileListAdapter;
import lazuardy.reza.teskomp2022.databinding.FragmentUserProfileListBinding;
import lazuardy.reza.teskomp2022.model.UserProfile;
import lazuardy.reza.teskomp2022.utils.MessageDialogFragment;
import lazuardy.reza.teskomp2022.viewmodel.UserProfileViewModel;
import lazuardy.reza.teskomp2022.viewmodel.UserProfileViewModelImpl;

/**
 * A simple {@link Fragment} subclass.
 */

public class UserProfileListFragment extends Fragment
        implements
        UserProfileListView, AdapterListener<UserProfile> {

    private FragmentUserProfileListBinding binding;
    private String keywordParam;
    private final static String POI = "poi";

    private UserProfileViewModel viewModel;
    private List<UserProfile> originalData;


    public static UserProfileListFragment newInstance() {
        UserProfileListFragment fragment = new UserProfileListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // note that the configuration sequence is important
        // 1. initialize view model
        // 2. configure views and
        // 3. observe / listen live data state
        // initialize view model
        ViewModelProvider provider = new ViewModelProvider(requireActivity());
        viewModel = provider.get(UserProfileViewModelImpl.class);


        // configure views
        binding.swipeRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                viewModel.refreshListUserProfile();
            }
        });
        binding.recycleView.setLayoutManager(new GridLayoutManager(getActivity(),1));


        // observe view state
        // on view state change, invoke apply() method,
        // which is defined by view interface
        //
        viewModel.getListUserProfile()
                .observe(getViewLifecycleOwner(), this::apply);
        viewModel.refreshListUserProfile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentUserProfileListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }


    @Override
    public void showData(List<UserProfile> profileList) {
        Log.d("priceinfoact","showContact "+keywordParam);
        originalData = profileList;

        UserProfileListAdapter adapter = new UserProfileListAdapter(profileList, this);
        binding.recycleView.setAdapter(adapter);
        binding.recycleView.setHasFixedSize(true);
    }

    @Override
    public void showProgress(boolean shown) {
        binding.swipeRefreshView.setRefreshing(shown);
    }

    @Override
    public void showMessage(String message) {
        if (isResumed()) {
            MessageDialogFragment dialogFragment = MessageDialogFragment.newInstance(message);
            dialogFragment.show(getParentFragmentManager(), "MessageDialogFragment");
        } else {

        }
    }


    @Override
    public void onItemClick(UserProfile data, int position) { ;
        Intent intent = DetailUserActivity.newIntent(getActivity(),data);
        startActivity(intent);
    }
}
