package lazuardy.reza.teskomp2022.repository;

import java.util.List;

import lazuardy.reza.teskomp2022.model.UserProfile;

public interface Repository {

    /** Get contact list from repository.
     *  When retrieving data done, listener will be invoked.
     *
     * @param listener
     */
    void getProfileList(RepositoryListener<List<UserProfile>> listener);

}
