package lazuardy.reza.teskomp2022.repository;

import android.app.Application;
import android.content.Context;

import java.util.Collections;
import java.util.List;

import lazuardy.reza.teskomp2022.apiservice.ErrorResponseDecoder;
import lazuardy.reza.teskomp2022.apiservice.NetworkService;
import lazuardy.reza.teskomp2022.apiservice.NetworkServicefactory;
import lazuardy.reza.teskomp2022.apiservice.ServiceListResponse;
import lazuardy.reza.teskomp2022.apiservice.SimpleCallback;
import lazuardy.reza.teskomp2022.model.UserProfile;
import retrofit2.Call;
import retrofit2.Response;

public class RepositoryImpl implements Repository {

    private final Application application;
    private final ErrorResponseDecoder decoder;

    public RepositoryImpl(Application application) {
        this.application = application;
        decoder = new ErrorResponseDecoder(application, Collections.emptyMap());
    }

    private NetworkService createService() {
        return NetworkServicefactory.createDefaultService(null, NetworkService.class);
    }

    @Override
    public void getProfileList(RepositoryListener<List<UserProfile>> listener) {
        Call<ServiceListResponse<UserProfile>> call = createService().getProfileList();
        call.enqueue(new SimpleCallback<ServiceListResponse<UserProfile>>() {

            @Override
            protected void onHttpResponseSuccess(
                    Call<ServiceListResponse<UserProfile>> call,
                    Response<ServiceListResponse<UserProfile>> response) {
                listener.onSuccess(response.body().getResults());
            }

            @Override
            protected void onHttpResponseFailed(
                    Call<ServiceListResponse<UserProfile>> call,
                    Response<ServiceListResponse<UserProfile>> response) {
                listener.onError(decoder.getErrorMessage(response));
            }

            @Override
            public void onFailure(Call<ServiceListResponse<UserProfile>> call, Throwable t) {
                listener.onError(decoder.getMessageFromRetrofitException(t));
            }

        });
    }
}
